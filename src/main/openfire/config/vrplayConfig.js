var app= angular.module('vrplay');


app.config(['$httpProvider', function($httpProvider) {
    $httpProvider.interceptors.push('HttpResponseErrorHandler');
    //$httpProvider.defaults.withCredentials = true;
}]);

app.factory('HttpResponseErrorHandler', ['$q', '$rootScope', '$timeout', function($q, $rootScope, $timeout) {
    return {
    	request: function(config) {
    		 $rootScope.ajaxon = true;
    		 return config;
    		 
    	    },
        response: function(response) {
        	 $rootScope.ajaxon = false;
           // alert('success: ' + response.status + ': ' + response.config.method + ': ' + response.config.url);
        	if(response && response.config && response.config.method == "POST" && response.status == 200){
        		if(response.data.reasonCode == "REQ_SUCCESS"){
        			$rootScope.responseStatus ="Success!"; 
        			$rootScope.isSuccessResponse=true;
        		}
        		else if(response.data.reasonCode == "REQ_FAILURE"){
        			$rootScope.responseStatus ="Error!";
        			$rootScope.isSuccessResponse=false;
        		}
        		$rootScope.responseDescription =response.data.reasonDescription; 
        	}
            return response;
        },
        responseError: function(rejection) {
//           // alert('rejected: ' + rejection.status + ': ' + rejection.config.method + ': ' + rejection.config.url);
        	$rootScope.responseStatus ="Error!";
        	$rootScope.responseDescription ="Oops, somthing went wrong"; 
        	$rootScope.isSuccessResponse=false;
        	 $rootScope.ajaxon = false;
            return $q.reject(rejection);
        }
    };
}]);


var app = angular.module('vrplay', ['ui.bootstrap','ngDraggable','angularUtils.directives.dirPagination','ngFileSaver']);

app.controller("vrplayController", function ($scope,$rootScope,vrplayService,$filter,$http,$window,FileSaver) {
	self = $scope;
	self.searchResult = [];
	self.searchRequest = {};
	self.login={};
	self.isResult=false;
	self.daySelected = {};
	self.isSearchenable= true;
	self.searchErrormsg= "";
	self.selectedRecord = [];
	self.todayDate=  $filter('date')(new Date(), "yyyy-MM-dd");
	$rootScope.isLoggedin = false;
	self.daySelectList=[{"id":1,"value":"Today"},{"id":2,"value":"This Week"},{"id":3,"value":"This Month"}];

	self.directionList=["","Inbound","Outbound"];
	self.reverse= true;
	self.selectedTab= null;
	self.configDetails = {"recorderType":"verint"};
	self.selectedRecords = [];
	
	self.getConfig = function(){
		return vrplayService.getConfig()
		.then(function(res){
			if(res){
				self.configDetails = res;
			}

		}, function(err){
		})



	};
	
	self.getNtrRecords = function(){
		self.searchErrormsg = "";

		var startDate;
		var endDate;
		var valid = true;

		self.searchRequest.startdatetime = document.getElementById('startDateTime').value +":00";
		self.searchRequest.enddatetime = document.getElementById('endDateTime').value +":00";
		



		return vrplayService.getNtrRecords($rootScope.loggedInUser,self.searchRequest,self.configDetails.niceRecorderApi)
		.then(function(res){
			if(res ){
				self.searchResult = res;
				angular.forEach(self.searchResult, function (value, key) { 
					if(value && !value.profileid){
						if(value && value.additionalProperty){
							angular.forEach(value.additionalProperty, function (val, key) {
								if(val.name == "CVSC00"){
									value.profileid = val.value; 
								}
							});
						}
					}
				});
				self.isResult = true;

			}else{
				self.isResult = false;
			}

		}, function(err){

		})

	};
	

	self.downloadNtrRecords = function(obj,action){
		
		return vrplayService.downloadNtrRecords($rootScope.loggedInUser,obj,self.configDetails.niceRecorderApi)
		.then(function(res){
			if(res ){
				if(res && res.VoiceRecording && res.VoiceRecording.vrdownloads && res.VoiceRecording.vrdownloads.vrdownload && res.VoiceRecording.vrdownloads.vrdownload.status == "Failed"){
					return null;
				}else if (res && res.VoiceRecording && res.VoiceRecording.vrdownloads && res.VoiceRecording.vrdownloads.vrdownload && res.VoiceRecording.vrdownloads.vrdownload.status == "Succesful"){
					angular.forEach(self.searchResult, function (value, key) { 
						if(value && obj.comid == value.comid){
							value.downloadpath =  self.configDetails.niceVoxApi+res.VoiceRecording.vrdownloads.vrdownload.audiofile;
							if(action == "playback"){
								self.StartMeUp(value);
							}else if(action == "download"){
								self.download(value);
							}
						}
					});
				}
			}else{
				return null;
			}

		}, function(err){

		})

	};
	

	self.getRecords = function(){
		self.searchErrormsg = "";
		if(self.daySelected.selected != null){
			if(self.daySelected.selected.id == 1){
				self.searchRequest.starttime = $filter('date')(new Date(), "yyyy-MM-dd");
				self.searchRequest.endtime = $filter('date')(new Date(), "yyyy-MM-dd");
			}else if(self.daySelected.selected.id == 2){
				var ourDate = new Date();
				var pastDate = ourDate.getDate() - 7;
				ourDate.setDate(pastDate);
				self.searchRequest.starttime = $filter('date')(ourDate, "yyyy-MM-dd");
				self.searchRequest.endtime = $filter('date')(new Date(), "yyyy-MM-dd");
			}else if(self.daySelected.selected.id == 3){
				var ourDate = new Date();
				var pastDate = ourDate.getDate() - 30;
				ourDate.setDate(pastDate);
				self.searchRequest.starttime = $filter('date')(ourDate, "yyyy-MM-dd");
				self.searchRequest.endtime = $filter('date')(new Date(), "yyyy-MM-dd");
			}
		}
		var startDate;
		var endDate;
		var valid = true;

		if(self.searchRequest.starttime){
			self.searchRequest.starttime = $filter('date')(self.searchRequest.starttime, "yyyy-MM-dd");
			startDate = new Date(self.searchRequest.starttime);
		}
		if(self.searchRequest.endtime){
			self.searchRequest.endtime = $filter('date')(self.searchRequest.endtime, "yyyy-MM-dd");
			endDate = new Date(self.searchRequest.endtime);
		}

		if(endDate && startDate){
			if(endDate < startDate){
				valid = false;
				self.isSearchenable = false;
				self.searchErrormsg= " End date should be lesser than start date.";
				return;
			}else if(endDate.getFullYear() !== startDate.getFullYear()){
				valid = false;
				self.isSearchenable = false;
				self.searchErrormsg= " please select date range in same year.";
				return;
			}
		}else{
			valid = false;
			self.isSearchenable = false;
			self.searchErrormsg= " Please select date range.";
			return;
		}


		if( valid && !self.isObjectEmpty(self.searchRequest) && self.isObjectValid(self.searchRequest , endDate, startDate)){
			self.isSearchenable = true;
			if(endDate){
				self.searchRequest.year = endDate.getFullYear();
			}else{
				self.searchRequest.year = new Date().getFullYear();
			}
			self.searchResult = [];
			return vrplayService.getRecords($rootScope.loggedInUser,self.searchRequest)
			.then(function(res){
				if(res && res.data && res.data.length > 0){
					self.searchResult = res.data;
					self.isResult = true;

				}else{
					self.isResult = false;
				}

			}, function(err){

			})
		}else{
			self.isSearchenable = false;
			if(self.searchErrormsg.length == 0){
				self.searchErrormsg = "Please provide valid input.(Agentname/Agentid/Account is required)";
			}
		}

	};

	$scope.download = function(obj) {
		var url = obj.downloadpath;
		 if(self.configDetails.recorderType != 'verint'){
			if(!obj.downloadpath){
				 self.downloadNtrRecords(obj,"download");
			} 
		 }
		var req = {
				url: url,
				method: 'GET',
				responseType: 'arraybuffer'

		};
		$http(req).then(function(resp){
			var serverData = new Blob([resp.data], {type: resp.headers()['content-type']});
			FileSaver.saveAs(serverData,"recorder.wav"); // File name set at server passed to the FileSaver function
		});
	}
	
	$scope.getLink = function(obj) {
		var url = obj.downloadpath;
		 if(self.configDetails.recorderType != 'verint'){
			if(!obj.downloadpath){
				 self.downloadNtrRecords(obj,"getlink");
			} 
		 }
	}

	self.isObjectEmpty = function(Obj){
		return Object.keys(Obj).length === 0;
	}

	self.isObjectValid = function(Obj, endDate, startDate){
		if((Obj.hasOwnProperty("account") && Obj.account) || (Obj.hasOwnProperty("agentid") && Obj.agentid) || (Obj.hasOwnProperty("agentname") && Obj.agentname)){
			return true;
		}else if((Obj.hasOwnProperty("calldirection") && Obj.calldirection) || (Obj.hasOwnProperty("dnis") && Obj.dnis) || (Obj.hasOwnProperty("ani") && Obj.ani)){
			var timeDiff = Math.abs(endDate.getTime() - startDate.getTime());
			var dayDifference = Math.ceil(timeDiff / (1000 * 3600 * 24));
			if(dayDifference <= 2){
				return true;
			}else{
				return false;
			}
		}
		return false;
	}

	self.sort = function(keyname){
		self.sortKey = "vrientJsonObject."+keyname;   //set the sortKey to the param passed
		self.reverse = !self.reverse; //if true make it false and vice versa
	}

	self.onLoggedIn = function(){
		$rootScope.ajaxon = true;
		return vrplayService.doLogin(self.login)
		.then(function(res){
			if(res){
				if(res && res.isLogin){
					$rootScope.loggedInUser = self.login.user;
					$rootScope.isLoggedin = true;
					$rootScope.isSupervisor = res.isSupervisor;
				}else{
					$rootScope.responseStatus ="Error!";
					$rootScope.isSuccessResponse=false;
					$rootScope.responseDescription ="Login failed";
				}
				$rootScope.ajaxon = false;
			}

		}, function(err){
			$rootScope.ajaxon = false;
		})



	};

	self.onLoggeddummy = function(){

		$rootScope.loggedInUser = self.login.user;
		$rootScope.isLoggedin = true;




	};

	self.getLogout = function(){
		$window.location.reload();
	}

	self.setActiveTab = function(tab) {
		self.selectedTab = tab
		self.searchRequest = {};
		self.isSearchenable = true;
		self.searchErrormsg= "";
		self.daySelected = {};
	}

	self.downloadRecords = function(){
		self.selectedRecord = true;
		$rootScope.ajaxon = true;
		var results = [];
		self.selectedRecords = [];
		angular.forEach(self.searchResult, function (value, key) { 
			if(value.vrientJsonObject.selected){
				results.push(value.vrientJsonObject); 
			}
		});
		if(results.length > 0){
			self.compresse_and_download(results,"records");
			$rootScope.ajaxon = false;
			
		}else{
			self.selectedRecord = false;
			$rootScope.ajaxon = false;
		}
	}
	
	self.exportRecords = function(){
		self.selectedRecord = true;
		$rootScope.ajaxon = true;
		self.selectedRecords = [];
		angular.forEach(self.searchResult, function (value, key) { 
			if(value.vrientJsonObject.selected){
				self.selectedRecords.push(value.vrientJsonObject); 
			}
		});
		
	}


	self.compresse_and_download = function(results,nombre) {
		var zip = new JSZip();
		var count = 0;
		var name = nombre+".zip";
		results.forEach(function(res){
			JSZipUtils.getBinaryContent(res.downloadpath, function (err, data) {
				if(err) {
					return; 
				}
				
				zip.file(res.path, data,  {binary:true});
				var b = new Blob([JSON.stringify(res)], {type : "application/json"});
				var jsonPath = res.path.split(".")[0]+".json";
				zip.file(jsonPath, b,  {binary:true});
				count++;
				if (count == results.length) {
					zip.generateAsync({type:'blob',platform:'win32'}).then(function(content) {
						saveAs(content, name);
					});
				}
			});
		});
	}
	
	 self.StartMeUp = function(obj)
	{	
		 if(self.configDetails.recorderType == 'verint'){
			 Player.URL = obj.downloadpath; 
		 }else{
			 if(obj && obj.downloadpath){
				 Player.URL = obj.downloadpath;
			 }else{
				 self.downloadNtrRecords(obj,"playback");
				 
			 }
		 }
	}
	 
	 
	 self.ShutMeDown = function()
	{
	    Player.controls.stop();
	}
	 
	 self.clearMessage = function(){
		 $rootScope.responseStatus ="";
     	$rootScope.responseDescription =""; 
	 }
	 

 
	
//	self.ConvertToCSV = function(objArray, headerList) { 
//		let array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray; 
//		let str = ''; 
//		let row = 'S.No, '; 
//		for (let index in headerList) { 
//			row += headerList[index] + ', '; 
//		} 
//		row = row.slice(0, -1); 
//		str += row + '\r\n'; 
//		for (let i = 0; i & lt; array.length; i++) { 
//			let line = (i + 1) + & #039;&# 039;; 
//			for (let index in headerList) { 
//				let head = headerList[index]; 
//				line += & #039;, &# 039; + array[i][head]; 
//			} 
//			str += line + & #039;\r\n&# 039;; 
//		} 
//		return str; 
//	} 

	 self.getConfig();

});

app.directive('jqdatepicker', function() {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function(scope, element, attrs, ngModelCtrl) {
            $(element).datepicker({
                dateFormat: 'yy-mm-dd',

                onSelect: function(date) {
                    var ngModelName = this.attributes['ng-model'].value;

                    // if value for the specified ngModel is a property of 
                    // another object on the scope
                    if (ngModelName.indexOf(".") != -1) {
                        var objAttributes = ngModelName.split(".");
                        var lastAttribute = objAttributes.pop();
                        var partialObjString = objAttributes.join(".");
                        var partialObj = eval("scope." + partialObjString);

                        partialObj[lastAttribute] = date;
                    }
                    // if value for the specified ngModel is directly on the scope
                    else {
                        scope[ngModelName] = date;
                    }
                    scope.$apply();
                }

            });
        }
    };
});

app.directive('jqdatetimepicker', function() {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function(scope, element, attrs, ngModelCtrl) {
            $(element).datetimepicker({
                format:'Y-m-d H:i',
				//datepicker:false,

               

            }); 
			
			
        }
    };
});







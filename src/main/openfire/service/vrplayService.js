var app= angular.module('vrplay');

app.service('vrplayService', function($http,$q,$rootScope,$location) {
	var vrplaySrv = this;
	vrplaySrv.hostname = $location.host();
	vrplaySrv.port = "7070";
	vrplaySrv.protocal = "http";
	
	
	vrplaySrv.getRecords = function(loggedIdUser,obj){
		var defer = $q.defer();
		var stringButtons = angular.toJson(obj);
		var posturl = vrplaySrv.protocal+"://"+vrplaySrv.hostname+":"+vrplaySrv.port+"/vrplay/cas/recorder/getRecords";
		$http({
			url:posturl,
			method:"POST",
			data:stringButtons,
			headers:{
				'Content-Type':'application/json'
			}
			
		}).
		
		success(function(res) {
			defer.resolve(res);
		}).error(function(err, status) {
			defer.reject(err);
		});
		return defer.promise;
	};
	
	vrplaySrv.getNtrRecords = function(loggedIdUser,obj,url){
		var defer = $q.defer();
		var stringButtons = angular.toJson(obj);
		if(obj && obj.startdatetime && obj.cvskey && obj.enddatetime){
			var posturl = url+"?action=3&starttime="+obj.startdatetime+"&endtime="+obj.enddatetime+"&cticallid="+obj.cvskey;
			
		}else if(obj && obj.startdatetime && obj.enddatetime){
			var posturl = url+"?action=3&starttime="+obj.startdatetime+"&endtime="+obj.enddatetime;
		}
		$http({
			url:posturl,
			method:"GET",
				headers:{
					'Accept':'*/*'
				}
			
		}).
		
		success(function(res) {
			defer.resolve(res);
		}).error(function(err, status) {
			defer.reject(err);
		});
		return defer.promise;
	};
	
	vrplaySrv.downloadNtrRecords = function(loggedIdUser,obj,url){
		var defer = $q.defer();
		var stringButtons = angular.toJson(obj);
		if(obj){
			var posturl = url+"?action=2&seqid="+obj.comid;
		}
		$http({
			url:posturl,
			method:"GET",
			headers:{
				'Accept':'*/*'
			},
			transformResponse: function (cnv) {
				var x2js = new X2JS();
				var aftCnv = x2js.xml_str2json(cnv);
				return aftCnv;
			}
			
		}).
		
		success(function(res) {
			defer.resolve(res);
		}).error(function(err, status) {
			defer.reject(err);
		});
		return defer.promise;
	};
	
	vrplaySrv.getConfig = function(){
		var defer = $q.defer();
		var posturl = vrplaySrv.protocal+"://"+vrplaySrv.hostname+":"+vrplaySrv.port+"/vrplay/cas/recorder/getConfig";
		$http({
			url:posturl,
			method:"POST",
			headers:{
				'Content-Type':'application/json'
			}
			
		}).
		
		success(function(res) {
			defer.resolve(res);
		}).error(function(err, status) {
			defer.reject(err);
		});
		return defer.promise;
	};
	
	vrplaySrv.doLogin = function(obj){
		var defer = $q.defer();
		var stringButtons = angular.toJson(obj);
		var posturl = vrplaySrv.protocal+"://"+vrplaySrv.hostname+":"+vrplaySrv.port+"/vrplay/cas/recorder/loginUser";
		$http({
			url:posturl,
			method:"POST",
			data:stringButtons,
			headers:{
				'Content-Type':'application/json'
			}
			
		}).
		
		success(function(res) {
			defer.resolve(res);
		}).error(function(err, status) {
			defer.reject(err);
		});
		return defer.promise;
	};
	
	
	
})
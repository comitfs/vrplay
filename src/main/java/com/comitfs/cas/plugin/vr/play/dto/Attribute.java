package com.comitfs.cas.plugin.vr.play.dto;

import java.io.Serializable;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlText;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Getter
@Setter
@NoArgsConstructor
public class Attribute implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String key;
	@JacksonXmlText
	private String value;
	

	
	
	
}

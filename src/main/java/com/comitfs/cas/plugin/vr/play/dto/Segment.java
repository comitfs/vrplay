package com.comitfs.cas.plugin.vr.play.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown=true)
public class Segment implements Serializable{

	private static final long serialVersionUID = 1L;
	private String contenttype;
	private String capturetype;
	private String starttime;
	//List<Tag> attributes;
	Attributes attributes;
	private String systemtype;
	Multipart MultipartObject;
	private String duration;


}
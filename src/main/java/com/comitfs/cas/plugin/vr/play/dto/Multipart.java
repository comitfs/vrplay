package com.comitfs.cas.plugin.vr.play.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
 public class Multipart implements Serializable{
 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
private String primary;


 // Getter Methods 

 public String getPrimary() {
  return primary;
 }

 // Setter Methods 

 public void setPrimary(String primary) {
  this.primary = primary;
 }
}
package com.comitfs.cas.plugin.vr.play.controller;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.comitfs.cas.plugin.vr.play.dto.CallMetaData;
import com.comitfs.cas.plugin.vr.play.dto.ConfigResponse;
import com.comitfs.cas.plugin.vr.play.dto.InjectRequest;
import com.comitfs.cas.plugin.vr.play.dto.LoginRequest;
import com.comitfs.cas.plugin.vr.play.dto.LoginResponse;
import com.comitfs.cas.plugin.vr.play.dto.SearchRequest;
import com.comitfs.cas.plugin.vr.play.service.RecordSchedulerServiceImpl;

import lombok.experimental.Accessors;

@Path("/recorder")
public class RecordSchedulerController {
	
	RecordSchedulerServiceImpl recordSchedulerServiceImpl = new RecordSchedulerServiceImpl();
	
	@POST
	@Path("/getRecords")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response getRecords( SearchRequest searchRequest) {
		
			
		CallMetaData callMetaData = recordSchedulerServiceImpl.getCallRecord(searchRequest);
		if(callMetaData == null) {
			return Response.status(Response.Status.BAD_REQUEST)
	                .entity(callMetaData).build();
		}
		
		return Response.status(Response.Status.OK)
                .entity(callMetaData).build();
		
	}
	
	@POST
	@Path("/loginUser")
	@Produces(MediaType.APPLICATION_JSON)
	public Response loginUsers( LoginRequest loginRequest) {
		
		LoginResponse loginResponse = recordSchedulerServiceImpl.doLogin(loginRequest);
		if(loginResponse == null) {
			return Response.status(Response.Status.BAD_REQUEST)
	                .entity(loginResponse).build();
		}
		
		return Response.status(Response.Status.OK)
                .entity(loginResponse).build();
		
	}
	
	@POST
	@Path("/getConfig")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getConfig( ) {
		
		ConfigResponse configResponse = recordSchedulerServiceImpl.getConfig();
		
		return Response.status(Response.Status.OK)
                .entity(configResponse).build();
		
	}
	
	@POST
	@Path("/injectRecords")
	@Produces(MediaType.APPLICATION_JSON)
	public Response injectRecords(InjectRequest injectRequest ) {
		
		if(injectRequest == null || injectRequest.getDirectoryName() == null) {
			return Response.status(Response.Status.BAD_REQUEST)
					.entity("No directoryName").build();
			
		}
		String response = recordSchedulerServiceImpl.injectRecords(injectRequest.getDirectoryName(), injectRequest.getElkIndex(), injectRequest.getElkType());
		
		return Response.status(Response.Status.OK)
                .entity(response).build();
		
	}
	
	
}

package com.comitfs.cas.plugin.vr.play.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
 public class Contact implements Serializable{

	private static final long serialVersionUID = 1L;
private String dnis;
 private String is_exception;
 private String number_of_transfers;
 private String duration;
 private String starttime;
 private String total_hold_time;
 private String ani;
 private String number_of_conferences;
 private String number_of_holds;
 private String time_offset;
 private String id;
 private String taggedby;


 // Getter Methods 

 public String getDnis() {
  return dnis;
 }

 public String getIs_exception() {
  return is_exception;
 }

 public String getNumber_of_transfers() {
  return number_of_transfers;
 }

 public String getDuration() {
  return duration;
 }

 public String getStarttime() {
  return starttime;
 }

 public String getTotal_hold_time() {
  return total_hold_time;
 }

 public String getAni() {
  return ani;
 }

 public String getNumber_of_conferences() {
  return number_of_conferences;
 }

 public String getNumber_of_holds() {
  return number_of_holds;
 }

 public String getTime_offset() {
  return time_offset;
 }

 public String getId() {
  return id;
 }

 public String getTaggedby() {
  return taggedby;
 }

 // Setter Methods 

 public void setDnis(String dnis) {
  this.dnis = dnis;
 }

 public void setIs_exception(String is_exception) {
  this.is_exception = is_exception;
 }

 public void setNumber_of_transfers(String number_of_transfers) {
  this.number_of_transfers = number_of_transfers;
 }

 public void setDuration(String duration) {
  this.duration = duration;
 }

 public void setStarttime(String starttime) {
  this.starttime = starttime;
 }

 public void setTotal_hold_time(String total_hold_time) {
  this.total_hold_time = total_hold_time;
 }

 public void setAni(String ani) {
  this.ani = ani;
 }

 public void setNumber_of_conferences(String number_of_conferences) {
  this.number_of_conferences = number_of_conferences;
 }

 public void setNumber_of_holds(String number_of_holds) {
  this.number_of_holds = number_of_holds;
 }

 public void setTime_offset(String time_offset) {
  this.time_offset = time_offset;
 }

 public void setId(String id) {
  this.id = id;
 }

 public void setTaggedby(String taggedby) {
  this.taggedby = taggedby;
 }
}
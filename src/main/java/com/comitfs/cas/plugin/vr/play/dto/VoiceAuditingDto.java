package com.comitfs.cas.plugin.vr.play.dto;

import java.sql.Timestamp;

public class VoiceAuditingDto {
	
	private Long cvskey;
	private Integer channel;
	private Timestamp startDate;
	private Timestamp endDate;
	private String period;
	private String direction;
	private String type;
	private String host;

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public Long getCvskey() {
		return cvskey;
	}
	public void setCvskey(Long cvskey) {
		this.cvskey = cvskey;
	}
	public Integer getChannel() {
		return channel;
	}
	public void setChannel(Integer channel) {
		this.channel = channel;
	}
	public Timestamp getStartDate() {
		return startDate;
	}
	public void setStartDate(Timestamp startDate) {
		this.startDate = startDate;
	}
	public Timestamp getEndDate() {
		return endDate;
	}
	public void setEndDate(Timestamp endDate) {
		this.endDate = endDate;
	}
	public String getPeriod() {
		return period;
	}
	public void setPeriod(String period) {
		this.period = period;
	}
	public String getDirection() {
		return direction;
	}
	public void setDirection(String direction) {
		this.direction = direction;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	
	

}

package com.comitfs.cas.plugin.vr.play.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@JsonIgnoreProperties(ignoreUnknown=true)
@Getter
@Setter
@NoArgsConstructor
public class Session implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String dnis;
	private String agent_id;
	private String pbx_login_id;
	private String ani;
	private String direction;
	private String wrapup_time;
	private String switch_id;
	private String switch_call_id;
	private String time_offset;
	private String extension;
	private String duration;
	private String total_hold_time;
	private String number_of_holds;
	@JsonIgnore
	private String stitching;
	private String id;
	private String taggedby;


}
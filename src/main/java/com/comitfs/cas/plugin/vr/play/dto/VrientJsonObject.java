package com.comitfs.cas.plugin.vr.play.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties
public class VrientJsonObject implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String ani;
	private String dnis;
	private String extension;
	private String timeonhold;
    private String extendedcallhistory;
    private String globalcallid;
    private String datasourceid;
    private String callingparty;
    private String calledparty;
    private String serialnumber;
    private String numberoftimestransferred;
    private String numberoftimesconference;
    private String pauseduration;
    private String numberofholds;
    private String parties;
    private String firedbusinessruleids;
    private String datasourceultraid;
    private String calldirection;
    private String firedbusinessrules;
    private String callid;
    private String datasourcename;
    private String devicename;
    private String numberdialed;
    private String calledpartyname;
    private String channelnumber;
    private String audiostarttime;
    private String stopreason;
    private String endtime;
    private String audioendtime;
    private String starttime;
    private String switch_call_id;
    private String switch_id;
    private String duration;
    private String agentname;
    private String agentid;
    private String account;
    private String file;
    private String downloadpath;
    private String path;
    private List<String> eventTypeList;
    private Boolean selected;
    
//	private String type;
//	private String profileid;
//    private String extendedcallhistory;
//    private String cscid;
//    private String sourceaddress;
//    private String destinationaddress;
//    private String direction;
//    private String comid;
//    private String sourcename;
//    private String vrchannel;
//    private String endtimestamp;
//    private String starttimestamp;
//    private String audioendtimestamp;
//    private String audiostarttimestamp;
//    private String duration;
//    private String file;
//    private List<String> eventTypeList;
//    private List<AdditionalProperty> additionalProperties;
//    
	
	public List<String> getEventTypeList(){
		if(eventTypeList == null) {
			eventTypeList = new ArrayList<String>();
		}
		
		return eventTypeList;
	}
	
//	public List<AdditionalProperty> getAdditionalProperties(){
//		if(additionalProperties == null) {
//			additionalProperties = new ArrayList<AdditionalProperty>();
//		}
//		
//		return additionalProperties;
//	}
	

}

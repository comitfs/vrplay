package com.comitfs.cas.plugin.vr.play.dto;

import java.io.Serializable;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlText;
public class Custom implements Serializable{
	private static final long serialVersionUID = 1L;
	private String key;
	@JacksonXmlText
	 private String text;


	 // Getter Methods 

	 public String getKey() {
	  return key;
	 }

	 public String getText() {
	  return text;
	 }

	 // Setter Methods 

	 public void setKey(String key) {
	  this.key = key;
	 }

	 public void setText(String text) {
	  this.text = text;
	 }
	}

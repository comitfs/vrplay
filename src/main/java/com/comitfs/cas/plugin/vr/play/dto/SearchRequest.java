package com.comitfs.cas.plugin.vr.play.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties
public class SearchRequest implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String ani;
	private String dnis;
	private String extension;
	private String callid;
	private String agentname;
    private String agentid;
    private String starttime;
    private String endtime;
    private String account;
    private String calldirection;
    private String channelnumber;
    private String callingparty;
    private String calledparty;
    private String timezone;
    private String year;
    private String recorderType;
    private String cvsKey;
}

package com.comitfs.cas.plugin.vr.play.dao;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import org.apache.log4j.Logger;
import org.jivesoftware.database.DbConnectionManager;


public class DbInsertJob {

    Logger log = Logger.getLogger(getClass().getName());


   
    
    public void updateXMLData(int status, String location) {
        Connection con = null;
        PreparedStatement stmt = null;
        long lastModifiedTime = System.currentTimeMillis();
        log.info(" -------------------------- " + location + "-------------------------------------- ");
        try {
            con = DbConnectionManager.getConnection();
            stmt = con.prepareStatement("UPDATE ofvrcsv set status=?,executeddate=? where location = ?");
            stmt.setInt(1, status);
            stmt.setTimestamp(2,new Timestamp(lastModifiedTime));
            stmt.setString(3, location);
            stmt.executeUpdate();
        } catch (SQLException sqe) {
            log.error("SQLException to update CSV file into Database :" + location, sqe);
        } catch (Exception e) {
            log.error("Failed to update CSV file into Database :" + location, e);
        }finally {
            DbConnectionManager.closeConnection(stmt,con);
        }
    }
    
   
    public void insertXMLDataBatch(List<File> fileList) {
        Connection con = null;
        PreparedStatement prepareStatement = null;

        try {
            con = DbConnectionManager.getConnection();
            prepareStatement = con.prepareStatement("INSERT INTO ofvrcsv(dirname,location) VALUES(?,?)");
            
            for(File file : fileList) {
            	String dirPath = file.getAbsolutePath();
            	String dirName = file.getName();
            	if (dirPath.contains("\\"))
            		dirPath = dirPath.replace("\\", "/");
            	prepareStatement.setString(1, dirName);
            	prepareStatement.setString(2, dirPath);
            	prepareStatement.addBatch();
            }
            prepareStatement.executeBatch();
        } catch (SQLException e) {
            log.error("Error while inserting csv file details in datbase: " + e.toString());
        } catch (Exception e) {
            log.error("Error while inserting csv file details in datbase: " + e.toString());
        } finally {
            DbConnectionManager.closeConnection(prepareStatement, con);
        }
    }
}


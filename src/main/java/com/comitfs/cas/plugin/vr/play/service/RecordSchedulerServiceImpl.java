package com.comitfs.cas.plugin.vr.play.service;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.log4j.Logger;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.jivesoftware.openfire.XMPPServer;
import org.jivesoftware.openfire.auth.AuthFactory;
import org.jivesoftware.openfire.group.Group;
import org.jivesoftware.openfire.group.GroupManager;
import org.jivesoftware.openfire.user.User;
import org.jivesoftware.openfire.user.UserManager;
import org.jivesoftware.util.JiveGlobals;

import com.alibaba.fastjson.JSON;
import com.comitfs.cas.plugin.vr.play.dto.CallMetaData;
import com.comitfs.cas.plugin.vr.play.dto.ConfigResponse;
import com.comitfs.cas.plugin.vr.play.dto.LoginRequest;
import com.comitfs.cas.plugin.vr.play.dto.LoginResponse;
import com.comitfs.cas.plugin.vr.play.dto.Root;
import com.comitfs.cas.plugin.vr.play.dto.SearchRequest;
import com.comitfs.cas.plugin.vr.play.dto.VrientJsonObject;
import com.comitfs.cas.plugin.vr.play.util.InjectThread;

public class RecordSchedulerServiceImpl implements RecordSchedulerService{

	ExecutorService service = Executors.newFixedThreadPool(1);


	Logger logger = Logger.getLogger(this.getClass());

	private String elkHostName = JiveGlobals.getProperty("vrplay.adaptor.elk.hostname", "localhost");
	private int elkPort = 	JiveGlobals.getIntProperty("vrplay.adaptor.elk.port", 9300);
	private String elkIndex = 	JiveGlobals.getProperty("vrplay.adaptor.elk.index", "vrient_index");
	private String elkType = 	JiveGlobals.getProperty("vrplay.adaptor.elk.type", "recorder1");
	private String webServer = JiveGlobals.getProperty("vrplay.web.server.url", "http://localhost:80");

	private TransportClient elkClient = null;

	@Override
	public List<CallMetaData> doSearch() {
		List<CallMetaData> searchResult = new ArrayList<CallMetaData>();



		return searchResult;
	}

	


	public CallMetaData getCallRecord(SearchRequest request) {
		CallMetaData callMetaData = new CallMetaData();
		try {
			if(request != null) {
				if(elkClient == null) {
					elkClient = new PreBuiltTransportClient(Settings.builder().put("client.transport.sniff", true).put("cluster.name","elasticsearch").build()).addTransportAddress(new TransportAddress(InetAddress.getByName(elkHostName), elkPort));
				}
				QueryBuilder queryBuilder = createSearchBuilder(request);
				if(queryBuilder != null) {
					logger.debug("searching in index " + "calls_"+request.getYear()+" type "+elkType);
//					SearchResponse response = elkClient.prepareSearch(elkIndex)
//					//SearchResponse response = elkClient.prepareSearch("calls_"+request.getYear())
//							.setTypes(elkType)
//							.setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
//							.setQuery(queryBuilder)
//							.execute()
//							.actionGet();
					
//					SearchRequestBuilder requestBuilder = elkClient.prepareSearch("calls_"+request.getYear())
//                            .setTypes(elkType)
//                            .setSize(100)
//                            .setQuery(queryBuilder); 
//					SearchHitIterator searchHitIterator = new SearchHitIterator(requestBuilder);
//					List<SearchHit> searchHits = new ArrayList<SearchHit>();
//					while (searchHitIterator.hasNext()) {
//						SearchHit hit = searchHitIterator.next();
//						searchHits.add(hit);
//					}
					List<SearchHit> searchHits = new ArrayList<SearchHit>();

					SearchResponse scrollResp = elkClient.prepareSearch(elkIndex)
					//SearchResponse scrollResp = elkClient.prepareSearch("calls_"+request.getYear())
					        .addSort(FieldSortBuilder.DOC_FIELD_NAME, SortOrder.ASC)
					        .setScroll(new TimeValue(60000))
					        .setQuery(queryBuilder)
					        .setSize(100).get(); //max of 100 hits will be returned for each scroll
					//Scroll until no hits are returned
					do {
					    for (SearchHit hit : scrollResp.getHits().getHits()) {
					    	searchHits.add(hit);
					    }

					    scrollResp = elkClient.prepareSearchScroll(scrollResp.getScrollId()).setScroll(new TimeValue(60000)).execute().actionGet();
					} while(scrollResp.getHits().getHits().length != 0);
//					List<SearchHit> searchHits = Arrays.asList(response.getHits().getHits());
					List<Root> results = new ArrayList<Root>();
					if(!searchHits.isEmpty()) {
						for(SearchHit searchHit : searchHits) {
							Root root = JSON.parseObject(searchHit.getSourceAsString(), Root.class);
							String downloadPath = getDownloadPath(root);
							root.getVrientJsonObject().setDownloadpath(downloadPath);
							root.getVrientJsonObject().setSelected(Boolean.FALSE);
							results.add(root);
						}
						callMetaData.setData(results);
					}else {
						return callMetaData;
					}
				}else {
					logger.info("Can't create a search builder " + "calls_"+request.getYear()+" type "+elkType);
					return null;
				}
			}else {
				logger.info("Bad request  " + "calls_"+request.getYear()+" type "+elkType);
				return null;
			}


		}catch(Exception e) {
			logger.error("Exception while getting records from ELK", e);
		}finally {
			if(elkClient != null) {
				elkClient.close();
			}
		}
		return callMetaData;

	}

	private String getDownloadPath(Root root) {

		try {
			if(root != null && root.getVrientJsonObject() != null ) {
				VrientJsonObject vrientJsonObject = root.getVrientJsonObject();
				String file = vrientJsonObject.getFile();
				if(file != null) {
					logger.info("file name "+ file);
					file = file.split("\\.")[0];
					file = file+".wav";
					logger.info("file name wav"+ file);
				}else {
					file = "";
				}
				String path = vrientJsonObject.getPath();
				if(path != null) {
					logger.info("path name "+ path);
					String[] patharr = path.split(":");
					logger.info("path name "+ patharr[1]);
					if(patharr.length > 0) {
						path = patharr[1];
					}else {
						path="/";
					}

				}else {
					path = "";
				}
				path = path.replace("\\\\", "\\");
				path = path.replace("\\", "/");
				logger.info("download path name "+ webServer+path+"/"+file);
				root.getVrientJsonObject().setPath(path+"/"+file);
				return webServer+path+"/"+file;
			}
		}catch(Exception e) {
			logger.error("Exception while getting download Path ", e);
		}
		return null;
	}

	private QueryBuilder createSearchBuilder(SearchRequest request) {
		BoolQueryBuilder queryBuilder = null;
		try {
			queryBuilder = QueryBuilders.boolQuery();
			if(request.getAccount() != null && request.getAccount().trim().length() > 0) {
				queryBuilder.must(QueryBuilders.matchQuery("vrientJsonObject.account", request.getAccount()));
			}
			if(request.getAni() != null && request.getAni().trim().length() > 0) {
				queryBuilder.must(QueryBuilders.matchQuery("vrientJsonObject.ani", request.getAni()));
			}
			if(request.getDnis() != null && request.getDnis().trim().length() > 0) {
				queryBuilder.must(QueryBuilders.matchQuery("vrientJsonObject.dnis", request.getDnis()));
			}
			if(request.getExtension() != null && request.getExtension().trim().length() > 0) {
				queryBuilder.must(QueryBuilders.matchQuery("vrientJsonObject.extension", request.getExtension()));
			}
			if(request.getCallid() != null && request.getCallid().trim().length() > 0) {
				queryBuilder.must(QueryBuilders.matchQuery("vrientJsonObject.callid", request.getCallid()));
			}
			if(request.getAgentname() != null && request.getAgentname().trim().length() > 0) {
				queryBuilder.must(QueryBuilders.matchQuery("vrientJsonObject.agentname", request.getAgentname()));
			}
			if(request.getAgentid() != null && request.getAgentid().trim().length() > 0) {
				queryBuilder.must(QueryBuilders.matchQuery("vrientJsonObject.agentid", request.getAgentid()));
			}
			if(request.getCalldirection() != null && request.getCalldirection().trim().length() > 0) {
				queryBuilder.must(QueryBuilders.matchQuery("vrientJsonObject.calldirection", request.getCalldirection()));
			}
			if(request.getChannelnumber() != null && request.getChannelnumber().trim().length() > 0) {
				queryBuilder.must(QueryBuilders.matchQuery("vrientJsonObject.channelnumber", request.getChannelnumber()));
			}
			if(request.getCallingparty() != null && request.getCallingparty().trim().length() > 0) {
				queryBuilder.must(QueryBuilders.matchQuery("vrientJsonObject.callingparty", request.getCallingparty()));
			}
			if(request.getStarttime() != null && request.getEndtime() != null) {
				queryBuilder.must(QueryBuilders.rangeQuery("vrientJsonObject.starttime").from(request.getStarttime()).to(request.getEndtime()));
			}
		} catch (Exception e) {
			logger.error("Exception while creating search builder ", e);
		}
		return queryBuilder;

	}

	@Override
	public LoginResponse doLogin(LoginRequest loginRequest) {
		LoginResponse loginResponse = new LoginResponse();
		if(loginRequest != null) {
		        try {
		            AuthFactory.authenticate(loginRequest.getUser(), loginRequest.getPassword());
		            User user = UserManager.getInstance().getUser(loginRequest.getUser().trim());
		            List<Group> groups = new ArrayList<Group>(GroupManager.getInstance().getGroups(user));
		            for(Group group : groups) {
		            	if(group.getName().equalsIgnoreCase(JiveGlobals.getProperty("vrplay.supervisor.group.name", "supervisorGroup"))) {
		            		loginResponse.setIsSupervisor(Boolean.TRUE);		
		            	}
		            }
		            loginResponse.setIsLogin(Boolean.TRUE);
		            
		        } catch (Exception e) {
		            logger.error("Invalid username (or) password of username "+loginRequest.getUser());
		            loginResponse.setIsLogin(Boolean.FALSE);
		            
		        }
		}else {
	        loginResponse.setIsLogin(Boolean.FALSE);
		}
		
		return loginResponse;
	}

	@Override
	public ConfigResponse getConfig() {
		ConfigResponse configResponse = new ConfigResponse();
		try {
			configResponse.setWebServer(JiveGlobals.getProperty("vrplay.web.server.url", "http://localhost:7070"));
			configResponse.setRecorderType(JiveGlobals.getProperty("vrplay.recorder.type", "ntr"));
			configResponse.setNiceRecorderApi(JiveGlobals.getProperty("vrplay.nice.api.url", "http://192.168.9.58:5050/vradaptor"));
			configResponse.setNiceVoxApi(JiveGlobals.getProperty("vrplay.nice.vox.url", "http://192.168.9.58:5050/vradaptor"));
			configResponse.setNiceVoxExtn(JiveGlobals.getProperty("vrplay.nice.vox.extn", ".wav"));
		} catch (Exception e) {
			logger.error("Exception while get config ", e);
		}
		return configResponse;
	}
	
	

	@Override
	public String injectRecords(String directoryName, String elkIndex, String elkType) {
		if(!directoryName.isEmpty()) {
			service.execute(new InjectThread(directoryName, elkIndex , elkType));
			return "injection started for "+ directoryName;
		}
		return null;
	}

}

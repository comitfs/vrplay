package com.comitfs.cas.plugin.vr.play.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@JsonIgnoreProperties(ignoreUnknown=true)
@Getter
@Setter
@NoArgsConstructor
public class Attributes  implements Serializable{

	private static final long serialVersionUID = 1L;
	private List<Tag> tag;
	







}
package com.comitfs.cas.plugin.vr.play;

import java.io.File;

import org.eclipse.jetty.server.handler.ContextHandlerCollection;
import org.eclipse.jetty.webapp.WebAppContext;
import org.jivesoftware.openfire.XMPPServer;
import org.jivesoftware.openfire.container.Plugin;
import org.jivesoftware.openfire.container.PluginManager;
import org.jivesoftware.openfire.http.HttpBindManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.comitfs.sso.AddSSOContext;

public class VRPlayplugin implements Plugin {
    private Logger logger = LoggerFactory.getLogger(VRPlayplugin.class);
    private static final String PLUGIN_NAME = "vrplay";
    private File pluginDirectory = null;

    @Override
    public void initializePlugin(PluginManager pluginManager, File file) {
        logger.info("Initializing IPC VRPLAYPlugin ....");
        this.pluginDirectory = file;
        createWebAppContext();
        AddSSOContext.getInstance().createSSOContextHandler("/vrplay/auth/");
    }

    @Override
    public void destroyPlugin() {

    }

    private void createWebAppContext() {
        try {
            logger.info("[VRPLAYPlugin] createWebAppContext - Creating web service ");

            ContextHandlerCollection contexts = HttpBindManager.getInstance().getContexts();

            try {
                WebAppContext context = new WebAppContext(contexts, pluginDirectory.getPath(), "/" + PLUGIN_NAME);
//                context.setContextPath("/ipcprov");
              //  context.setWelcomeFiles(new String[]{"vrplay.html"});
//                context.addServlet("org.glassfish.jersey.servlet.ServletContainer", "com.comitfs.cas.plugin.vr.play.controller");
//                context.
            } catch (Exception e) {
                logger.error("Error creating webapp context", e);
            }
        } catch (Exception e) {
            logger.error("Error creating webapp context", e);
        }
    }
}

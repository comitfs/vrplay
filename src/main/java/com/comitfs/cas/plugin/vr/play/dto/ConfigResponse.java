package com.comitfs.cas.plugin.vr.play.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties
public class ConfigResponse {
	private String webServer;
	private String recorderType;
	private String niceRecorderApi;
	private String niceVoxApi;
	private String niceVoxExtn;
	private List<String> headers;
	
}

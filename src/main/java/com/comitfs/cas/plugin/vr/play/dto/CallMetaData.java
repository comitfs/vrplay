package com.comitfs.cas.plugin.vr.play.dto;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class CallMetaData implements Serializable{
	

	private static final long serialVersionUID = 1L;
	
	private List<Root> data;

}

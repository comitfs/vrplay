package com.comitfs.cas.plugin.vr.play.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Root implements Serializable{

	private static final long serialVersionUID = 1L;
	VrientJsonObject vrientJsonObject;
}

package com.comitfs.cas.plugin.vr.play.service;

import java.util.List;

import com.comitfs.cas.plugin.vr.play.dto.CallMetaData;
import com.comitfs.cas.plugin.vr.play.dto.ConfigResponse;
import com.comitfs.cas.plugin.vr.play.dto.LoginRequest;
import com.comitfs.cas.plugin.vr.play.dto.LoginResponse;

public interface RecordSchedulerService {
	public List<CallMetaData> doSearch();
	public LoginResponse doLogin(LoginRequest loginRequest);
	public ConfigResponse getConfig();
	public String injectRecords(String directoryName, String elkIndex, String elkType);
}

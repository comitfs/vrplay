package com.comitfs.cas.plugin.vr.play.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Setter
@Getter
public class LoginResponse {
	private Boolean isSupervisor;
	private Boolean isLogin;
}

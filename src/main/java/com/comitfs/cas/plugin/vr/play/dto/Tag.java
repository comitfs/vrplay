package com.comitfs.cas.plugin.vr.play.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Setter
@Getter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown=true)
public class Tag implements Serializable{
	private static final long serialVersionUID = 1L;
	private List<Attribute> attribute;
	 private String taggedby;
	 private String timestamp;


	}

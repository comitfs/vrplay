package com.comitfs.cas.plugin.vr.play.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@JsonIgnoreProperties(ignoreUnknown=true)
@Getter
@Setter
@NoArgsConstructor
public class Recording implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String finalized;
	private String master;
	private String keepflag;
	private String rollbackrequired;
	private String rollbacktime;
	private String compressiontype;
	private String compressed;
	private String rollbackid;
	Segment segment;
	Session session;
	Contact contact;
	String xmlns;
	String version;
	String ref;


}
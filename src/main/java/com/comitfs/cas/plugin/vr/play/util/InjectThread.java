package com.comitfs.cas.plugin.vr.play.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.elasticsearch.action.bulk.BulkItemResponse;
import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.jivesoftware.util.JiveGlobals;

import com.comitfs.cas.plugin.vr.play.dao.DbInsertJob;
import com.comitfs.cas.plugin.vr.play.dto.AdditionalProperty;
import com.comitfs.cas.plugin.vr.play.dto.Attribute;
import com.comitfs.cas.plugin.vr.play.dto.Recording;
import com.comitfs.cas.plugin.vr.play.dto.Root;
import com.comitfs.cas.plugin.vr.play.dto.Segment;
import com.comitfs.cas.plugin.vr.play.dto.Session;
import com.comitfs.cas.plugin.vr.play.dto.Tag;
import com.comitfs.cas.plugin.vr.play.dto.VrientJsonObject;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

public class InjectThread implements Runnable{

	private static final String CALLINGPARTY = "callingparty";
	private static final String DATASOURCEID = "datasourceid";
	private static final String GLOBALCALLID = "globalcallid";
	private static final String EXTENDEDCALLHISTORY = "extendedcallhistory";
	private static final String TIMEONHOLD = "timeonhold";
	private static final String EXTENSION = "extension";
	private static final String DNIS = "dnis";
	private static final String ANI = "ani";
	private static final String USERID = "userid";
	private static final String CALLID = "callid";
	private static final String CALLDIRECTION = "calldirection";


	Logger logger = Logger.getLogger(this.getClass());
	DbInsertJob dbInsertJob = new DbInsertJob();

	private String elkHostName = JiveGlobals.getProperty("vrplay.adaptor.elk.hostname", "localhost");
	private int elkPort = 	JiveGlobals.getIntProperty("vrplay.adaptor.elk.port", 9300);
	private String elkIndex = 	JiveGlobals.getProperty("vrplay.adaptor.elk.index", "vrient_index");
	private String elkType = 	JiveGlobals.getProperty("vrplay.adaptor.elk.type", "recorder1");

	private TransportClient elkClient = null;

	private String directoryName;



	

	public InjectThread(String directoryName,String elkIndex, String elkType) {
		super();
		this.elkIndex = elkIndex;
		this.elkType = elkType;
		this.directoryName = directoryName;
	}

	@Override
	public void run() {
		try {
			List<File> fileListInParent = new ArrayList<File>(); 
			List<File> directoryList = new ArrayList<File>(); 
			fileListInParent = this.listFilesAndDirectories(directoryName,fileListInParent,directoryList);
			logger.info("files in "+directoryName+ " "+ fileListInParent.size());
			logger.info("directory in "+directoryName+ " "+ directoryList.size());
			if(!fileListInParent.isEmpty())
				this.insertCallRecord(fileListInParent, directoryName);

			logger.info("*************  loading started "+ new Date() +" ****** "+ directoryName);
			for(File directory: directoryList) {
				if(directory.exists()) {
					logger.info("*************  directory started "+ new Date() +" ****** "+ directory.getName());
					List<File> fileList = new ArrayList<File>(); 
					fileList = this.listFiles(directory.getAbsolutePath(),fileList);
					this.insertCallRecord(fileList, directory.getName());
				}
				logger.info("*************  directory ended "+ new Date() +" ****** "+ directory.getName());
			}

			logger.info("***** loading end *********** "+ new Date() +" **** "+directoryName);

		}catch(Exception e) {
			logger.error("Exception in ivoking scheduler for directory "+ directoryName , e);
		}

	}

	public List<File>  listFilesAndDirectories(String directoryName, List<File> fileList , List<File> directoryList) {
		try {
			File directory = new File(directoryName);
			if(directory.isDirectory()) {
				File[] fList = directory.listFiles();
				if(fList != null)
					for (File file : fList) {
						if (file.isFile() && file.getName().endsWith(".xml")) {
							fileList.add(file);
						} else if (file.isDirectory()) {
							directoryList.add(file);
						}
					}
			}else {
				logger.info("not a directory "+ directoryName);
			}
		}catch(Exception e) {
			logger.error("Exception while listing file " , e);
		}
		return fileList;
	}

	public List<File>  listFiles(String directoryName, List<File> fileList) {
		try {
			File directory = new File(directoryName);
			if(directory.isDirectory()) {
				File[] fList = directory.listFiles();
				if(fList != null)
					for (File file : fList) {
						if (file.isFile() && file.getName().endsWith(".xml")) {
							fileList.add(file);
						} else if (file.isDirectory()) {
							listFiles(file.getAbsolutePath(),fileList);
						}
					}
			}else {
				logger.info("not a directory ");
			}
		}catch(Exception e) {
			logger.error("Exception while listing file " , e);
		}
		return fileList;
	}

	public void insertCallRecord(List<File> fileList, String directoryName) {
		try {
			dbInsertJob.insertXMLDataBatch(fileList);
			logger.debug("Insert complete into DB "+ directoryName);
			this.processFiles(fileList);
		}catch(Exception e) {
			logger.error("Exception while inserting call records" , e);
		}
	}

	public void updateCallRecord(int status, String filePath) {
		String absolutePath = null;
		try {
			if(filePath != null) {
				absolutePath = filePath.replace("\\", "/");
			}
			if(absolutePath != null) {
				dbInsertJob.updateXMLData(status, absolutePath);
			}
		}catch(Exception e) {
			logger.error("Exception while updating call records" , e);
		}
	}


	public void processFiles(List<File> fileList) {
		XmlMapper xmlMapper = new XmlMapper();
		try {
			logger.debug("in process File and push recording");
			if(elkClient == null) {
				elkClient = new PreBuiltTransportClient(Settings.builder().put("client.transport.sniff", true).put("cluster.name","elasticsearch").build()).addTransportAddress(new TransportAddress(InetAddress.getByName(elkHostName), elkPort));
			}
			Map<String,String> jsonList = new HashMap<String, String>();
			for(File file : fileList) {
				if(file.isFile() && file.getName().endsWith(".xml")) {
					String toConvertXMl =  processFile(file);  
					xmlMapper.setDefaultUseWrapper(false);
					Recording recording = xmlMapper.readValue(toConvertXMl, Recording.class);
					VrientJsonObject vrientJsonObject = createVrientJson(recording, file.getName(), file.getParent());
					Root root = new Root();
					root.setVrientJsonObject(vrientJsonObject);
					ObjectMapper mapper = new ObjectMapper();
					String json = mapper.writeValueAsString(root);
					logger.debug("processed xml into string" + json);
					jsonList.put(vrientJsonObject.getFile(), json);
				}
			}
			pushRecordToElk(jsonList);
		} catch (Exception e) {
			logger.error("Exception while create ELK client ", e);
		}

	}

	public void pushRecordToElk(Map<String,String> jsonList) {

		try {
			BulkRequestBuilder bulkRequest = elkClient.prepareBulk();
			for (Map.Entry<String, String> entry : jsonList.entrySet()) {
				bulkRequest.add(elkClient.prepareIndex(elkIndex, elkType)
						.setSource(entry.getValue(), XContentType.JSON));
			}
			BulkResponse bulkResponse = bulkRequest.get();
			if (bulkResponse.hasFailures()) {
				for(int i = 0; i < bulkResponse.getItems().length; i++) {
					BulkItemResponse bulkItemResponse = bulkResponse.getItems()[i];
					if( bulkItemResponse.isFailed()) {
						IndexRequest ireq = (IndexRequest) bulkRequest.request().requests().get(i);
						Map<String, Object> objMap = ireq.sourceAsMap();
						String json =  objMap.get("vrientJsonObject").toString();
						String absoultePath = getAbsoultePath(json);
						logger.debug("elk failed file  "+ absoultePath);
						updateCallRecord(1, absoultePath);
					}
				}
			}
		}catch(Throwable e) {
			logger.error("Exception while pushing data to ELK ", e);
		}
	}

	private String getAbsoultePath(String read) {
		String path = null;
		String file =  null;
		String pathName = null;
		String fileName =  null;
		if(read != null) {
			String pathSubstring = read.substring((read.indexOf("path")));
			String fileSubstring = read.substring((read.indexOf("file")));
			if(pathSubstring != null) {
				path = pathSubstring.substring(0,(pathSubstring.indexOf(",")));
				pathName = path.substring(path.indexOf("=")+1);
			}
			if(fileSubstring != null) {
				file = fileSubstring.substring(0,(fileSubstring.indexOf(",")));
				fileName = file.substring(file.indexOf("=")+1);
			}
		}

		return pathName+"\\"+fileName;
	}



	private String processFile(File file) {
		String result = null;
		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(file));
			String line;
			StringBuilder sb = new StringBuilder();
			br.readLine();
			while((line=br.readLine())!= null){
				sb.append(line.trim());

			}
			result = sb.toString();
			result = result.replaceAll("x:", "");
			result = result.replaceAll(":x", "");
		} catch (Exception e) {
			logger.error("Exception while process file " + file.getName() , e);
		}

		return result;
	}

	public  VrientJsonObject createVrientJson(Recording recording, String fileName, String path) {
		VrientJsonObject vrientJsonObject = new VrientJsonObject();
		try {
			vrientJsonObject.setFile(fileName);
			vrientJsonObject.setPath(path);
			if(recording != null ) {
				Segment segment = recording.getSegment(); 
				if(segment != null && segment.getAttributes() != null) {
					vrientJsonObject.setStarttime(recording.getSegment().getStarttime());
					List<Tag> tags = segment.getAttributes().getTag();
					if(!tags.isEmpty()) {
						for(Tag tag : tags) {
							if(tag != null) {
								List<Attribute> attributes = tag.getAttribute();
								if(attributes != null && !attributes.isEmpty()) {
									for(Attribute attribute : attributes) {
										if(attribute != null) {
											if(attribute.getKey().equalsIgnoreCase(CALLID)) {
												vrientJsonObject.setCallid(attribute.getValue());
											}
											if(attribute.getKey().equalsIgnoreCase(ANI)) {
												vrientJsonObject.setAni(attribute.getValue());
											}
											if(attribute.getKey().equalsIgnoreCase(DNIS)) {
												vrientJsonObject.setDnis(attribute.getValue());
											}
											if(attribute.getKey().equalsIgnoreCase(EXTENSION)) {
												vrientJsonObject.setExtension(attribute.getValue());
											}
											if(attribute.getKey().equalsIgnoreCase(TIMEONHOLD)) {
												vrientJsonObject.setTimeonhold(attribute.getValue());
											}
											if(attribute.getKey().equalsIgnoreCase(EXTENDEDCALLHISTORY)) {
												vrientJsonObject.setExtendedcallhistory(attribute.getValue());
											}
											if(attribute.getKey().equalsIgnoreCase(GLOBALCALLID)) {
												vrientJsonObject.setGlobalcallid(attribute.getValue());
											}
											if(attribute.getKey().equalsIgnoreCase(DATASOURCEID)) {
												vrientJsonObject.setDatasourceid(attribute.getValue());
											}
											if(attribute.getKey().equalsIgnoreCase(CALLINGPARTY)) {
												vrientJsonObject.setCallingparty(attribute.getValue());
											}
											if(attribute.getKey().equalsIgnoreCase("calledparty")) {
												vrientJsonObject.setCalledparty(attribute.getValue());
											}
											if(attribute.getKey().equalsIgnoreCase("serialnumber")) {
												vrientJsonObject.setSerialnumber(attribute.getValue());
											}
											if(attribute.getKey().equalsIgnoreCase("numberoftimestransferred")) {
												vrientJsonObject.setNumberoftimestransferred(attribute.getValue());
											}
											if(attribute.getKey().equalsIgnoreCase("numberoftimesconference")) {
												vrientJsonObject.setNumberoftimesconference(attribute.getValue());
											}
											if(attribute.getKey().equalsIgnoreCase("pauseduration")) {
												vrientJsonObject.setPauseduration(attribute.getValue());
											}
											if(attribute.getKey().equalsIgnoreCase("numberofholds")) {
												vrientJsonObject.setNumberofholds(attribute.getValue());
											}
											if(attribute.getKey().equalsIgnoreCase("pauseduration")) {
												vrientJsonObject.setPauseduration(attribute.getValue());
											}
											if(attribute.getKey().equalsIgnoreCase("datasourceultraid")) {
												vrientJsonObject.setDatasourceultraid(attribute.getValue());
											}
											if(attribute.getKey().equalsIgnoreCase("calldirection")) {
												vrientJsonObject.setCalldirection(attribute.getValue());
											}
											if(attribute.getKey().equalsIgnoreCase("datasourcename")) {
												vrientJsonObject.setDatasourcename(attribute.getValue());
											}
											if(attribute.getKey().equalsIgnoreCase("devicename")) {
												vrientJsonObject.setDevicename(attribute.getValue());
											}
											if(attribute.getKey().equalsIgnoreCase("numberdialed")) {
												vrientJsonObject.setNumberdialed(attribute.getValue());
											}
											if(attribute.getKey().equalsIgnoreCase("calledpartyname")) {
												vrientJsonObject.setCalledpartyname(attribute.getValue());
											}
											if(attribute.getKey().equalsIgnoreCase("channelnumber")) {
												vrientJsonObject.setChannelnumber(attribute.getValue());
											}
											if(attribute.getKey().equalsIgnoreCase("stopreason")) {
												vrientJsonObject.setStopreason(attribute.getValue());
											}
											if(attribute.getKey().equalsIgnoreCase("endtime")) {
												vrientJsonObject.setEndtime(attribute.getValue());
											}
											if(attribute.getKey().equalsIgnoreCase("audioendtime")) {
												vrientJsonObject.setAudioendtime(attribute.getValue());
											}
											if(attribute.getKey().equalsIgnoreCase("audiostarttime")) {
												vrientJsonObject.setAudiostarttime(attribute.getValue());
											}
											if(attribute.getKey().equalsIgnoreCase("agentname")) {
												vrientJsonObject.setAgentname(attribute.getValue());
											}
											if(attribute.getKey().equalsIgnoreCase("agentid")) {
												vrientJsonObject.setAgentid(attribute.getValue());
											}
											if(attribute.getKey().equalsIgnoreCase("account")) {
												vrientJsonObject.setAccount(attribute.getValue());
											}
											if(attribute.getKey().equalsIgnoreCase("eventtype")) {
												vrientJsonObject.getEventTypeList().add(attribute.getValue());
											}
										}
									}
								}
							}
						}
					}
				}

				Session session = recording.getSession();
				if(session != null) {
					vrientJsonObject.setSwitch_call_id(session.getSwitch_call_id());
					vrientJsonObject.setSwitch_id(session.getSwitch_id());
					vrientJsonObject.setDuration(session.getDuration());

				}

			}
		}catch(Exception e) {
			logger.error("Exception while creating verint Object ", e);
		}
		return vrientJsonObject;
	}


	//	public  VrientJsonObject createVrientJson(Recording recording, String fileName, String path) {
	//		VrientJsonObject vrientJsonObject = new VrientJsonObject();
	//		try {
	//			
	//			if(recording != null ) {
	//				Segment segment = recording.getSegment(); 
	//				if(segment != null && segment.getAttributes() != null) {
	//					vrientJsonObject.setStarttimestamp(recording.getSegment().getStarttime());
	//					List<Tag> tags = segment.getAttributes().getTag();
	//					if(!tags.isEmpty()) {
	//						for(Tag tag : tags) {
	//							if(tag != null) {
	//								List<Attribute> attributes = tag.getAttribute();
	//								if(attributes != null && !attributes.isEmpty()) {
	//									for(Attribute attribute : attributes) {
	//										if(attribute != null) {
	//											if(attribute.getKey().equalsIgnoreCase(CALLID)) {
	//												vrientJsonObject.setComid(attribute.getValue());
	//											}
	//											if(attribute.getKey().equalsIgnoreCase(EXTENSION)) {
	//												vrientJsonObject.setProfileid(attribute.getValue());
	//											}
	//											if(attribute.getKey().equalsIgnoreCase(EXTENDEDCALLHISTORY)) {
	//												vrientJsonObject.setExtendedcallhistory(attribute.getValue());
	//											}
	//											if(attribute.getKey().equalsIgnoreCase(GLOBALCALLID)) {
	//												vrientJsonObject.setCscid(attribute.getValue());
	//											}
	//											if(attribute.getKey().equalsIgnoreCase(CALLINGPARTY)) {
	//												vrientJsonObject.setSourceaddress(attribute.getValue());
	//											}
	//											if(attribute.getKey().equalsIgnoreCase("calledparty")) {
	//												vrientJsonObject.setDestinationaddress(attribute.getValue());
	//											}
	//											if(attribute.getKey().equalsIgnoreCase("calldirection")) {
	//												vrientJsonObject.setDirection(attribute.getValue());
	//											}
	//											if(attribute.getKey().equalsIgnoreCase("calledpartyname")) {
	//												vrientJsonObject.setSourcename(attribute.getValue());
	//											}
	//											if(attribute.getKey().equalsIgnoreCase("channelnumber")) {
	//												vrientJsonObject.setVrchannel(attribute.getValue());
	//											}
	//											if(attribute.getKey().equalsIgnoreCase("endtime")) {
	//												vrientJsonObject.setEndtimestamp(attribute.getValue());
	//											}
	//											if(attribute.getKey().equalsIgnoreCase("audioendtime")) {
	//												 vrientJsonObject.setAudioendtimestamp(attribute.getValue());
	//											}
	//											if(attribute.getKey().equalsIgnoreCase("audiostarttime")) {
	//												vrientJsonObject.setAudiostarttimestamp(attribute.getValue());
	//											}
	//											if(attribute.getKey().equalsIgnoreCase("eventtype")) {
	//												vrientJsonObject.getEventTypeList().add(attribute.getValue());
	//											}
	//											if(attribute.getKey().equalsIgnoreCase(ANI) || attribute.getKey().equalsIgnoreCase(TIMEONHOLD) || attribute.getKey().equalsIgnoreCase(DNIS)
	//													|| attribute.getKey().equalsIgnoreCase(DATASOURCEID) || attribute.getKey().equalsIgnoreCase("serialnumber") || attribute.getKey().equalsIgnoreCase("numberoftimestransferred")
	//													|| attribute.getKey().equalsIgnoreCase("numberoftimesconference") || attribute.getKey().equalsIgnoreCase("pauseduration") || attribute.getKey().equalsIgnoreCase("numberofholds")
	//													|| attribute.getKey().equalsIgnoreCase("pauseduration") || attribute.getKey().equalsIgnoreCase("datasourceultraid") || attribute.getKey().equalsIgnoreCase("datasourcename") 
	//													|| attribute.getKey().equalsIgnoreCase("devicename") || attribute.getKey().equalsIgnoreCase("numberdialed") || attribute.getKey().equalsIgnoreCase("stopreason")
	//													|| attribute.getKey().equalsIgnoreCase("agentname") || attribute.getKey().equalsIgnoreCase("agentid")
	//													|| attribute.getKey().equalsIgnoreCase("account")) {
	//												vrientJsonObject.getAdditionalProperties().add(getAdditionalProperty(attribute));
	//											}
	//										}
	//									}
	//								}
	//							}
	//						}
	//					}
	//				}
	//
	//				Session session = recording.getSession();
	//				if(session != null) {
	//					vrientJsonObject.getAdditionalProperties().add(getAdditionalProperty("switch_call_id", session.getSwitch_call_id()));
	//					vrientJsonObject.getAdditionalProperties().add(getAdditionalProperty("switch_id", session.getSwitch_id()));
	//					vrientJsonObject.setDuration(session.getDuration());
	//
	//				}
	//
	//			}
	//			vrientJsonObject.getAdditionalProperties().add(getAdditionalProperty("path", path));
	//			vrientJsonObject.setFile(fileName);
	//		}catch(Exception e) {
	//			logger.error("Exception while creating verint Object ", e);
	//		}
	//		return vrientJsonObject;
	//	}

//	private AdditionalProperty getAdditionalProperty(Attribute attribute) {
//		AdditionalProperty additionalProperty = new AdditionalProperty();
//		additionalProperty.setName(attribute.getKey());
//		additionalProperty.setValue(attribute.getValue());
//		return additionalProperty;
//	}
//
//	private AdditionalProperty getAdditionalProperty(String key, String value) {
//		AdditionalProperty additionalProperty = new AdditionalProperty();
//		additionalProperty.setName(key);
//		additionalProperty.setValue(value);
//		return additionalProperty;
//	}




}
